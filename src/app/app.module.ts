import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { EmployeeService } from './services/employee.service';
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ShowComponent } from './show/show.component';

import { TableModule } from "primeng/table";
import { ButtonModule } from 'primeng/button';
import { AddComponent } from './add/add.component';
import { InputTextModule } from 'primeng/inputtext';
import { DetailsComponent } from './details/details.component';
import { EditComponent } from './edit/edit.component';
@NgModule({
  declarations: [
    AppComponent,
    ShowComponent,
    AddComponent,
    DetailsComponent,
    EditComponent
  ],

  imports: [
    TableModule,
    ButtonModule,
    InputTextModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule ,
    AppRoutingModule,
    
 
  ],
  providers: [EmployeeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
