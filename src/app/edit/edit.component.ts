import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  currentEmployee = null;
  message = '';
  id: number;
  constructor(private employeeService: EmployeeService, private route: ActivatedRoute,
    private router: Router) { }
  ngOnInit() {
    this.message = '';
    //  this.getEmployee(this.route.snapshot.paramMap.get('id'));
    this.id = +this.route.snapshot.paramMap.get('id');
  }

  getEmployee(id) {
    this.employeeService.get(id)
      .subscribe(
        (data: any) => {
          debugger;
          this.currentEmployee = data.data;

        },
        error => {
          console.log(error);
        });
  }




  deleteEmployee() {
    alert("متد حذف فراخوانی شد");
    this.employeeService.delete(this.id)
      .subscribe(
        response => {
          console.log(response);
          // this.router.navigate(['/show']);
          this.message = "کارمند با موفقیت حذف شد";
        },
        error => {
          console.log(error);
        });
  }
}
