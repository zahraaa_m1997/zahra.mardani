import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { Employee } from '../models/employee';

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {
  employees: Employee[];
  constructor(private employeeService: EmployeeService) { }


  ngOnInit(): void {
    this.retrieveEmployees();
  }

  retrieveEmployees() {
    this.employeeService.getAll()
      .subscribe(
      res => {
          this.employees = res.data;
        },
        error => {
     
        });
  }


}
