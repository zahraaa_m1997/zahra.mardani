import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
const baseUrl = 'http://dummy.restapiexample.com/api/v1';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get(`${baseUrl}/employees`);
  }

  get(id) {
    return this.http.get(`${baseUrl}/employee/${id}`);
  }

  create(data) {
    return this.http.post(`${baseUrl}/create`, data);
  }

  update(id, data) {
    return this.http.put(`${baseUrl}/${id}`, data);
  }

  delete(id) {
    return this.http.delete(`${baseUrl}/delete/${id}`);
  }

 
}
